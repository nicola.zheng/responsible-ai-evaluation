let expanded = false;
var loading = false; // Initialize loading state
var clientSelected = "";
var clientModelSelected = "";
var inputQuestionSelected = "";
var externalEndpointSelected = "";


function showStep(step) {
    document.getElementById('step4').classList.add('hidden');
    document.getElementById('step0').classList.add('hidden');
    document.getElementById('step1').classList.add('hidden');
    document.getElementById('step2').classList.add('hidden');
    document.getElementById('step3').classList.add('hidden');
    document.getElementById('step' + step).classList.remove('hidden');
    // Show/hide Run Evaluation button based on the current step and loading state
    //console.log(step)
    //console.log(loading)
    var button = document.getElementById('evaluate-button');
    var clientSelect = document.getElementById('client-select');
    var clientModelSelect = document.getElementById('domain-select');
    var inputQuestionSelect = document.getElementById('input-question');
    var externalEndpointSelect = document.getElementById('ext-endpoint');

    clientSelected = clientSelect.value;
    clientModelSelected = clientModelSelect.value;
    inputQuestionSelected = inputQuestionSelect.value;
    externalEndpointSelected = externalEndpointSelect.value;

    if (step === 2 && !loading) {
        //console.log("in if condition")
        button.style.display = 'block';
        button.innerHTML = 'Run Evaluation'; // Reset button text
        button.disabled = false;
    }
    // If showing step 3, update the content with the selected client
    if (step === 3) {
        var now = new Date(); // Get the current timestamp
        var formattedTimestamp = now.toLocaleString(); // Format the timestamp as needed
        document.getElementById('timestamp').textContent = formattedTimestamp;
        loading = false;
        console.log(clientSelected)
        console.log(clientModelSelected)
        console.log(inputQuestionSelected)
        console.log(externalEndpointSelected)

        document.getElementById('client-selected').textContent = clientSelected;
        document.getElementById('domain-selected').textContent = clientModelSelected;
        document.getElementById('input-questioned').textContent = inputQuestionSelected;
        document.getElementById('ext-endpoint').textContent = externalEndpointSelected;
    }
}

function setLoading() {
    loading = true; // Set loading state to true
    var button = document.getElementById('evaluate-button');
    button.innerHTML = '<svg class="spinner mr-3 w-6 h-6 inline-block" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"><circle class="opacity-25" cx="12" cy="12" r="10" stroke="currentColor" stroke-width="4"></circle><path class="opacity-75" fill="currentColor" d="M4 12a8 8 0 018-8V0C5.373 0 0 5.373 0 12h4zm2 5.291A8.001 8.001 0 0120 12h4a12 12 0 00-12-12V2a10 10 0 00-1.978 19.646L6 17.291z"></path></svg>Loading...';
    button.disabled = true;
}

function expandAll() {
    const detailBodies = document.querySelectorAll('#evaluation-results .hidden');
    const icons = document.querySelectorAll('#evaluation-results .arrow-icon');
    detailBodies.forEach(body => {
        body.classList.remove('hidden');
    });
    icons.forEach(icon => {
        icon.classList.add('rotate');
    });
    expanded = true;
}

function collapseAll() {
    const detailBodies = document.querySelectorAll('#evaluation-results .grid-cols-2');
    const icons = document.querySelectorAll('#evaluation-results .arrow-icon');
    detailBodies.forEach(body => {
        body.classList.add('hidden');
    });
    icons.forEach(icon => {
        icon.classList.remove('rotate');
    });
    expanded = false;
}

function hideButtonsOnStep3() {
    var element = document.getElementById('buttons-on-step-3');
    if (element) {
        element.style.display = 'none';
    }
}

function showButtonsOnStep3() {
    var element = document.getElementById('buttons-on-step-3');
    if (element) {
        element.style.display = 'block'; // or 'inline' or any other display value you need
    }
}

function expandAndDownload() {
    expandAll();
    hideButtonsOnStep3();
    setTimeout(() => {
        downloadPDF();
        setTimeout(() => {
            collapseAll();
            showButtonsOnStep3();
        }, 5); // Adjust delay as needed
    }, 5); // Adjust delay as needed
}

function downloadPDF() {
    var element = document.getElementById('step3');
    var opt = {
        margin: 0.8,
        filename: 'Evaluation_Results.pdf',
        image: { type: 'jpeg', quality: 0.98 },
        html2canvas: { scale: 2 },
        
    };
    html2pdf().set(opt).from(element).save().then(() => {
        document.body.removeChild(tempContainer); // Remove the temporary container
        if (expanded) {
            collapseAll();
        }
    });
}

        
// Triggerable Button
function checkInputs() {
    const evaluateButton = document.getElementById('evaluate-button');
    const clientSelect = document.getElementById('client-select');
    const clientModel = document.getElementById('domain-select');
    const externalEndpoint = document.getElementById('ext-endpoint');
    const inputQuestion = document.getElementById('input-question');
    const nextOnStep1 = document.getElementById('next-on-step-1');

    
    if ((clientSelect.value || externalEndpoint.value) && inputQuestion.value) {
        evaluateButton.disabled = false;
        evaluateButton.classList.remove('bg-gray-500');
        evaluateButton.classList.add('bg-violet-700', 'hover:bg-violet-700');
        nextOnStep1.disabled = false;
        nextOnStep1.classList.remove('bg-gray-500');
        nextOnStep1.classList.add('bg-violet-700', 'hover:bg-violet-700');
    } else {
        evaluateButton.disabled = true;
        evaluateButton.classList.remove('bg-violet-700', 'hover:bg-violet-700');
        evaluateButton.classList.add('bg-gray-500');
        nextOnStep1.disabled = true;
        nextOnStep1.classList.remove('bg-violet-700', 'hover:bg-violet-700');
        nextOnStep1.classList.add('bg-gray-500');
    }
};

document.addEventListener('htmx:afterOnLoad', function (event) {
    var button = document.getElementById('evaluate-button');
    if (event.detail.xhr.responseURL.endsWith('/evaluate')) {
        loading = false;
        //showFireworks();
        showStep(3);
        button.innerHTML = 'Run Evaluation';
        button.disabled = false;
    }
});

document.addEventListener("htmx:afterRequest", function(event) {
    if (event.detail.xhr.status === 200) {
        let response = event.detail.xhr.responseText;
        let loginResponseElement = document.getElementById('login-response');
        if (response === 'OK') {
            loginResponseElement.innerText = '';
            loginResponseElement.classList.add('text-violet-700');
            document.querySelector('.splash-screen').style.display = 'flex'; // Show splash screen
            setTimeout(function() {
                document.querySelector('.splash-screen').style.display = 'none';
                showStep(0);
            }, 1500); // Adjust time as needed
        } else if (response === 'Invalid password') {
            loginResponseElement.innerText = 'Invalid password';
            loginResponseElement.classList.add('text-red-600');
        } else if (response === 'Invalid username') {
            loginResponseElement.innerText = 'Invalid username';
            loginResponseElement.classList.add('text-red-600');
        }
    }
});