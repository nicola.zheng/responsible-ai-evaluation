from langchain.prompts import PromptTemplate
from backend.utils import llm_model

def ask(question:str):
    prompt = PromptTemplate(
        template=f'Question: {question} \
        Answer shortly:',
    input_variables=["question"],
    )    
    llm = llm_model.load_model()
    qa_chain = prompt | llm

    #print(f'call_openai.ask question: {question}')
    response = qa_chain.invoke({'question':question})
    #print(f'call_openai.ask response: {response.content}')
    return response.content