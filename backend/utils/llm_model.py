from langchain_openai import AzureChatOpenAI
from dotenv import load_dotenv
import os
from backend.utils.read_secret_manager import get_secret_value, read_azure_keys
load_dotenv()

secret_dict = get_secret_value(secret_name="azure_keys")
api_base= read_azure_keys(secret_dict=secret_dict, key="api_base")
api_key= read_azure_keys(secret_dict=secret_dict, key="api_key")
api_version= read_azure_keys(secret_dict=secret_dict, key="api_version")

def load_model():
    llm = AzureChatOpenAI(
        azure_endpoint=api_base,
        api_key=api_key,
        azure_deployment="gpt-35-turbo",
        api_version=api_version,
        temperature=0
    )
    return llm