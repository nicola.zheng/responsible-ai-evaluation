from langchain.prompts import PromptTemplate
from backend.utils import llm_model
from dotenv import load_dotenv
load_dotenv()


def rephrase(question_to_rephrase:str, n_sentence:int = 4, model_name:str = 'gpt-3.5-turbo'):
    task = f"""Follow the steps:
1) Read the sentence: ```{question_to_rephrase}```
2) Rephrase it in {n_sentence} different sentences
3) Use only the new line to separate the sentences avoiding bullet points
"""
#2) Rephrase it in {n_sentence} different sentences changing language
    prompt = PromptTemplate(
        template=""" 
    Sentence: {sentence}
    Question: {question}

    Answer:""",
        input_variables=["question"],
    )

    llm = llm_model.load_model()

    qa_chain = prompt | llm
    response = qa_chain.invoke({'question': task, 'sentence':question_to_rephrase})
    response_text = response.content
    question_list = response_text.split('\n')
    if '' in question_list:
        question_list.remove('')
    if None in question_list:
        question_list.remove(None)
    return question_list
