import requests
import sys
sys.path.append('.')
from backend.utils.read_secret_manager import get_secret_value, read_intelligpt_login

secret_dict = get_secret_value(secret_name="intelligpt_login")
intelligpt_token = read_intelligpt_login(secret_dict=secret_dict, key="INTELLIGPT_TOKEN")
intelligpt_url= read_intelligpt_login(secret_dict=secret_dict, key="INTELLIGPT_QA_ENDPOINT")

CONTEXT_SEP = '\n'

class EndpointGetAnswer():
   def __init__(
      self,
      endpoint_url,
      token:str = intelligpt_token,
      context: str = 'afast',
      domain: str = 'general',
      ):
      self.token = token
      self.endpoint_url = endpoint_url
      self.context = context
      self.domain = domain
   
   def get_answer(self, question:str):
      request_body = {
        "question": question
      }
         
      print(f'Request to {self.endpoint_url}: {request_body}')
      if self.endpoint_url == intelligpt_url:
        header_request = {
          "Content-Type": "application/json",
          "Accept": "application/json",
          "Authorization": "Bearer {token}".format(token=self.token)
        }
        request_body["client"] = self.context
        request_body["domain"] = self.domain
        request_body["command"] = "Answer in english with at least 30 words"
        request_body["session_id"] = ""        
      
        response = requests.post(self.endpoint_url, json=request_body, headers=header_request)
        print(f'Response of GenAI endpoint: {response.json()}')
        endpoint_response_answer = response.json().get("answer")
        endpoint_response_context = response.json().get("body")
        endpoint_response_title = response.json().get("titles")
        endpoint_response_section = response.json().get("sections")
      else:
        response = requests.post(self.endpoint_url, json=request_body)
        endpoint_response_answer = response.json().get("response").get("answer")
        endpoint_response_context = response.json().get("response").get("body")
        endpoint_response_title = response.json().get("response").get("title")
        endpoint_response_section = response.json().get("response").get("section")

      if endpoint_response_context is None or endpoint_response_context =='' or endpoint_response_context == []:
         endpoint_response_context = ['no context']
      elif type(endpoint_response_context)==str:
        endpoint_response_context = endpoint_response_context.split(CONTEXT_SEP)
        endpoint_response_context = list(set(endpoint_response_context))
        endpoint_response_context.remove('')

      return endpoint_response_answer, endpoint_response_context, endpoint_response_title, endpoint_response_section
    
if __name__=="__main__":

  endpoint = EndpointGetAnswer(
    endpoint_url=intelligpt_url,
    token=intelligpt_token,
    context='accenture',
    domain='general'
  )
  print(endpoint.get_answer(question="cosa fa accenture?"))