import os
import json
from google.oauth2 import service_account
from google.cloud import secretmanager


def get_service_account_credentials():
    if local_machine():
        credentials = service_account.Credentials.from_service_account_file(
            'acn-bigdatalabmi-platform-77916d049527.json')
        return credentials
    else:
        return None

def local_machine():
    is_local = not "GAE_" in str(os.environ.keys())
    return is_local

def get_secret_value(secret_name, version="latest"):
    """
    Retrieves a secret from Google Cloud Secret Manager.

    Args:
        secret_name (str): The name of the secret in Secret Manager.
        version (str, optional): The version of the secret to access. Defaults to "latest".

    Returns:
        str: The secret value as a string.
    """

    # Create the Secret Manager client
    client = secretmanager.SecretManagerServiceClient(credentials=get_service_account_credentials())

    # Build the secret name based on project and secret
    project_id = "acn-bigdatalabmi-platform"
    name = client.secret_version_path(project_id, secret_name, version)

    try:
        # Access the secret version
        response = client.access_secret_version(request={"name": name})
        # Decode the secret payload
        secret_payload = response.payload.data.decode("UTF-8")
        return json.loads(secret_payload)
    except Exception as e:
        raise Exception(f"Error accessing secret: {e}")
    
def read_azure_keys(secret_dict: dict, key:str):
    if secret_dict:
        openai_api_key = secret_dict.get(key)
        if openai_api_key:
            return openai_api_key
        else:
            print(f"Secret dictionary doesn't contain a {key} key")
            return None
    else:
        print("Failed to retrieve secret_dict")
        return None
    
def read_login_credentials(secret_dict: dict, htmx_username:str, htmx_password:str):
    if secret_dict:
        username_list = secret_dict.get("username")
        password = secret_dict.get("password")
        return username_list, password
    else:
        print("Failed to retrieve secret_dict")
        return None

def read_intelligpt_login(secret_dict: dict, key:str):
    if secret_dict:
        secret_key = secret_dict.get(key)
        if secret_key:
            return secret_key
        else:
            print(f"Secret dictionary doesn't contain a {key} key")
            return None
    else:
        print("Failed to retrieve secret_dict")
        return None
        

    
"""
secret_dict = get_secret_value(secret_name="azure_keys")
api_base= read_azure_keys(secret_dict=secret_dict, key="api_base")
api_key= read_azure_keys(secret_dict=secret_dict, key="api_key")
api_version= read_azure_keys(secret_dict=secret_dict, key="api_version")

print(f'api_base: {api_base}\n api_key: {api_key}\n api_version: {api_version}')
"""