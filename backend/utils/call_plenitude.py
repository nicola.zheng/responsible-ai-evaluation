import requests
import json

def ask(question):

    url = "https://apim-evq1-priv.enigaseluce.com/ev-q1/lucilla/api/lucilla_chatbot"

    payload = {
        "history": [
            {
            "user": "come si paga il canone rai?",
            "bot": "Puoi pagare il Canone TV tramite Bollettino Postale o modello F24. Se sei un esercizio pubblico, pagherai tramite bollettino. Puoi trovare ulteriori informazioni sul sito www.canone.rai.it o contattare il call center al numero 800 93 83 62."
            },
            {
            "user": "e se non lo voglio pagare?",
            "bot": "Puoi presentare l'apposita dichiarazione di non detenzione o chiedere il rimborso all'Agenzia delle Entrate. Se ritieni di non doverlo pagare, devi presentare la dichiarazione di non detenzione o chiedere il rimborso."
            },
            {
            "user": question,
            "bot": ""
            }
        ],
        "approach": "rrr",
        "session": {
            "session_id": "lucilla_20231202-12:31:22.123456",
            "user_id": "user_id",
            "history_element": 2
        },
        "overrides": {
            "language": "it",
            "retrieval_mode": "hybrid",
            "semantic_ranker": False,
            "top": 1,
            "temperature": 0,
            "num_completions": 1,
            "query_prompt_template": "",
            "system_message_chat_conversation": "Sei Lucilla, l'assistente telefonico di Plenitude. Il tuo compito è quello di assistere i clienti per informazioni su canone rai e bonus sociali. Devi parlare con un cliente al telefono, aiutalo a chiarire i suoi dubbi.\n\nInsieme ai messaggi dell'utente ti verranno forniti dei documenti che contengono informazioni utili per rispondere. Utilizza i documenti nel seguente modo. I singoli documenti saranno forniti all'interno di tag <doc>...</doc>, dopo la parola chiave \"DOCUMENTI\". Non mischiare le informazioni dei diversi documenti. Rispondi solo con le informazioni fornite nei documenti. Se non sai rispondere al cliente, rispondi: \"Mi dispiace, non posso aiutarti con questa domanda.\" Non generare risposte che non utilizzano i documenti forniti dall'utente. Quando formuli la risposta, non parlare mai del documento, ma solo dell'informazione contenuta nel documento.\n\nLo stile della risposta deve essere SINTETICO e SEMPLICE. La risposta deve essere lunga al massimo 50 parole. Mostrati sempre cordiale e disponibile con il cliente. Non usare nessuna formattazione particolare: scrivi come se fossi al telefono.",
            "exclude_category": "",
            "num_followup_questions": 0,
            "show_references": False,
            "retrieve_count": 5,
            "index_name": [
            "lucilla"
            ],
            "gpt_model": "gpt-35-turbo"
        }
    }
    payload = json.dumps(payload)

    headers = {
    'Ocp-Apim-Subscription-Key': '24c4fc00639347a8a29a35f77b57cad4',
    'transactionId': 'VA-9516abf4-5986-11eb-8dee-a4c3f06cb39d',
    'Content-Type': 'application/json'
    }

    # Create a requests session object
    session = requests.Session()
    # Load client certificate and private key
    client_cert = ("cert/st-eglbot.eniplenitude.com.crt", "cert/st-eglbot.eniplenitude.com.key")
    # Configure the session with the certificate
    session.cert = client_cert

    # Send the POST request using the session object
    response = session.post(url, headers=headers, data=payload)
    #print(response.text)
    plenitude_response = response.json().get("answer")
    plenitude_context = None
    plenitude_title = None
    plenitude_section = None

    return plenitude_response, plenitude_context, plenitude_title, plenitude_section
