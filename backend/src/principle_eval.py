from backend.utils import rephrase_with_llm, call_plenitude
from backend.utils.call_endpoint import EndpointGetAnswer
from backend.utils.read_secret_manager import get_secret_value, read_intelligpt_login
from backend.src import criteria_eval
from backend.config.question_augmentation_config import fairness_example_list, privacy_example_list, soundness_example_list
from backend.config.principles_config import *
from multiprocessing import Pool, cpu_count, dummy
from concurrent.futures import ThreadPoolExecutor, as_completed
import numpy as np

class PrincipleEval:
    def __init__(
        self,
        uuid:str,
        principles:list,
        question:str,
        client_qa:str=None,
        domain_qa:str=None,
        get_answer_endpoint:str = None
        ):
        self.uuid = uuid
        self.principles = principles
        self.question = question
        self.client_qa = client_qa
        self.domain_qa = domain_qa
        self.get_answer_endpoint = get_answer_endpoint


    def question_generation(self, principle:str=None, sub_principle:str=None, question_to_test:str=None):
        question_list = [] if question_to_test is None else [question_to_test]
        if principle == 'FAIRNESS':
            if sub_principle == 'EQUITY':
                question_list += [example + question_to_test for example in fairness_example_list]
            if sub_principle == 'HARMFULNESS':
                question_list == question_list
            if sub_principle == 'FAITHFULNESS':
                question_list == question_list
        if principle == 'PRIVACY':
            if sub_principle == 'SENSITIVE INFORMATION':
                question_list = privacy_example_list
        if principle == 'ROBUSTNESS':
            if sub_principle == "CONSISTENCY":
                question_list = rephrase_with_llm.rephrase(question_to_test)
        if principle == 'SECURITY':
            if sub_principle == "PROMPT INJECTION":
                question_list = [f"{CRITERIA_DICT.get(principle).get(sub_principle).get('langchain_criteria')}"]
            if sub_principle == "PROMPT LEAKING":
                question_list  = [f"{CRITERIA_DICT.get(principle).get(sub_principle).get('langchain_criteria')}"]
            if sub_principle == "JAILBREAKING":
              question_list = [f"{CRITERIA_DICT.get(principle).get(sub_principle).get('langchain_criteria')}"]
        if principle == 'SOUNDNESS':
            if sub_principle == "COHERENCE":
                question_list = soundness_example_list
        if principle == 'TRASPARANCY':
            if sub_principle == 'QUOTES':
                question_list = question_list
        return question_list

    def get_answer(self, question:str):
        if self.get_answer_endpoint:
            external_endpoint = EndpointGetAnswer(endpoint_url=self.get_answer_endpoint)
        else:
            secret_dict = get_secret_value(secret_name="intelligpt_login")
            intelligpt_url= read_intelligpt_login(secret_dict=secret_dict, key="INTELLIGPT_QA_ENDPOINT")
            intelligpt_token= read_intelligpt_login(secret_dict=secret_dict, key="INTELLIGPT_TOKEN")
            print(f"Intelligpt URL: {intelligpt_url}")
            print(f"Intelligpt Token: {intelligpt_token}")
            print(f"Client: {self.client_qa}")
            print(f"Domain: {self.domain_qa}")
            external_endpoint = EndpointGetAnswer(
                endpoint_url=intelligpt_url, 
                token=intelligpt_token,
                context=self.client_qa,
                domain=self.domain_qa
            )
        
        if self.client_qa=="plenitude":
            response, context, title, section = call_plenitude.ask(question=question)
        else:
            response, context, title, section = external_endpoint.get_answer(question=question)
        return response, context, title, section
    
    def eval_sub_principle(
            self,
            question_list:list,
            answer_list:list,
            context_list:list = None,
            title_list:list = None,
            section_list:list = None,
            principle:str=None,
            sub_principle:str=None,
            ):
        
        score = '0'
        eval_score_list = []

        if (principle, sub_principle) in PRINCIPLES_ENSEMBLE_QUESTION_EVAL:
            answer, eval_score = criteria_eval.ensemble_eval(
                question_list=question_list,
                answer_list=answer_list,
                principle=principle,
                sub_principle=sub_principle
            )
            print(f'[{self.uuid}] score: {eval_score}')
            eval_score_list.append(eval_score)

        if (principle, sub_principle) in PRINCIPLES_CRITERIA_EVAL_QUESTION:
            for i in range(min(len(question_list), len(answer_list))):
                question = question_list[i]
                answer = answer_list[i]
                answer, eval_score = criteria_eval.langchain_criteria_eval(
                    question=question,
                    answer=answer,
                    principle=principle,
                    sub_principle=sub_principle
                )
                print(f'[{self.uuid}] score: {eval_score}')
                eval_score_list.append(eval_score)

        if (principle, sub_principle) in PRINCIPLES_REJECTING_QUESTION:
            for i in range(min(len(question_list), len(answer_list))):
                question = question_list[i]
                answer = answer_list[i]
                answer, eval_score = criteria_eval.rejection_eval(
                    question=question,
                    answer=answer,
                    principle=principle,
                    sub_principle=sub_principle
                )
                print(f'[{self.uuid}] score: {eval_score}')
                eval_score_list.append(eval_score)     
            
        if (principle, sub_principle) in PRINCIPLES_WITH_RAGAS_EVAL:
            answer, eval_score = criteria_eval.ragas_metric_eval(
                question_list=question_list,
                answer_list=answer_list,
                context_list=context_list)
            print(f'[{self.uuid}] score: {eval_score}')
            eval_score_list.append(eval_score)
        
        if (principle, sub_principle) in TRANSPARENCY_PRINCIPLE:
            answer, eval_score = criteria_eval.transparancy_eval(
                question_list=question_list,
                answer_list=answer_list,
                sub_principle=sub_principle,
                context_list=context_list,
                title_list=title_list,
                section_list=section_list
            )
            print(f'[{self.uuid}] score: {eval_score}')
            eval_score_list.append(eval_score)
        
        score = round(np.mean(eval_score_list), 2)             
        return answer, score, eval_score_list
        
    def eval_principle(
            self,
            principle
    ):
        results_dict = {}
        results_dict['score'] = []

        for sub_principle in CRITERIA_DICT.get(principle).keys():
            print(principle, "-", sub_principle)

            question_list_test = self.question_generation(
                question_to_test=self.question,
                principle=principle,
                sub_principle=sub_principle)
            answer_list_test = []
            context_list_test = []
            title_list_test = []
            section_list_test = []
            for query in question_list_test:
                answer, context, title, section = self.get_answer(question=query)
                answer_list_test.append(answer)
                context_list_test.append(context)
                title_list_test.append(title)
                section_list_test.append(section)
                print(
                f"""
        question: {query}
        answer: {answer}
                """)

            results_dict[sub_principle] = {}

            answer, score, eval_score_list = self.eval_sub_principle(
                question_list=question_list_test, 
                context_list=context_list_test,
                answer_list=answer_list_test,
                title_list=title_list_test,
                section_list=section_list_test,
                principle=principle,
                sub_principle=sub_principle
            )
            results_dict[sub_principle]['type'] = CRITERIA_DICT.get(principle).get(sub_principle).get('type')
            results_dict[sub_principle]['method'] = CRITERIA_DICT.get(principle).get(sub_principle).get('eval_method')
            results_dict[sub_principle]['criteria'] = CRITERIA_DICT.get(principle).get(sub_principle).get('eval_criteria')
            results_dict[sub_principle]['question'] = question_list_test

            if principle=="TRANSPARENCY":
                results_dict[sub_principle]['answer'] = answer
            else:
                results_dict[sub_principle]['answer'] = answer_list_test
            eval_score_list = [int(score*100) for score in eval_score_list]
            results_dict[sub_principle]['score_list_single_question'] = eval_score_list
            results_dict[sub_principle]['score'] = int(score*100)
            results_dict['score'] += [score]
            if score <1:
                results_dict[sub_principle]['mitigation'] = CRITERIA_DICT.get(principle).get(sub_principle).get('mitigation')
            else:
                results_dict[sub_principle]['mitigation'] ="No action required"
        results_dict['score'] = int(np.mean(results_dict['score'])*100)
        return results_dict

    def multiprocess(self):
        self.principles.sort()
        results = dict.fromkeys(self.principles)
        result_for_htmx = {principle: {} for principle in self.principles}
        """
        n_process = cpu_count()
        n_process = min(n_process, len(self.principles))
        p = Pool(n_process)
        #p = Pool(4)
        #p = dummy.Pool(4)
        result_list = p.map(self.eval_principle, self.principles)
        for i, principle in enumerate(self.principles):
            result_for_htmx[principle] = result_list[i]
        return result_for_htmx
        """
        n_threads = min(len(self.principles), 4)  # Use 4 or cpu_count() as needed
        with ThreadPoolExecutor(max_workers=n_threads) as executor:
            future_to_principle = {executor.submit(self.eval_principle, principle): principle for principle in self.principles}
            for future in as_completed(future_to_principle):
                principle = future_to_principle[future]
                try:
                    result_for_htmx[principle] = future.result()
                except Exception as exc:
                    print(f'{principle} generated an exception: {exc}')
        
        return result_for_htmx
    
    def run(self) -> dict:
        import time
        start = time.time()
        print("Starting Eval")
        result = self.multiprocess()
        end = time.time()
        print("Finished Eval")
        print(f"Time elapsed: {round(end - start, 2)} s")
        return result
        
