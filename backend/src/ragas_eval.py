import sys
sys.path.append('.')
from backend.utils.read_secret_manager import get_secret_value, read_azure_keys

from langchain_openai.chat_models import AzureChatOpenAI
from langchain_openai.embeddings import AzureOpenAIEmbeddings

from ragas.llms import LangchainLLMWrapper
from ragas.dataset_schema import SingleTurnSample 
from ragas.metrics import Faithfulness


secret_dict = get_secret_value(secret_name="azure_keys")
api_base= read_azure_keys(secret_dict=secret_dict, key="api_base")
api_key= read_azure_keys(secret_dict=secret_dict, key="api_key")
api_version= read_azure_keys(secret_dict=secret_dict, key="api_version")

class Evaluation:
    def __init__(self):
        self.azure_model = AzureChatOpenAI(
            openai_api_version='2023-05-15',
            azure_endpoint=api_base,
            openai_api_key=api_key,
            openai_api_type="azure",
            azure_deployment="gpt-4o-mini",
            model="gpt-4o-mini",
            validate_base_url=False,
        )
        self.llm = LangchainLLMWrapper(self.azure_model)
        self.azure_embeddings = AzureOpenAIEmbeddings(
            openai_api_version='2023-05-15',
            azure_endpoint=api_base,
            openai_api_key=api_key,
            openai_api_type="azure",
            azure_deployment="text-embedding-ada-002-v2",
            model="text-embedding-ada-002",
        )
        
    def evaluate(
            self,
            question:str=None,
            answer:str=None,
            context:str=None,
        ) -> float:
        sample = SingleTurnSample(
            user_input=str(question),
            response=str(answer),
            retrieved_contexts=context
        )
        scorer = Faithfulness(llm=self.llm)
        return(scorer.single_turn_score(sample))

if __name__ == "__main__":

    ragas_eval = Evaluation()
    dataset_dict = {
        'question': ["Che cos'è Accenture?"],
        'answer': ["Accenture è una società di consulenza e servizi professionali."], 
        'contexts': [["Accenture è una società di consulenza e servizi professionali."]]
    }
    ragas_eval.evaluate(
        question=dataset_dict['question'][0],
        answer=dataset_dict['answer'][0],
        context=dataset_dict['contexts'][0]   
    )