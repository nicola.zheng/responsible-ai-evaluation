from backend.config.principles_config import EVALUATION_MODEL, CRITERIA_DICT
from backend.utils import call_openai, llm_model
from langchain.evaluation import load_evaluator
from backend.utils.read_secret_manager import get_secret_value, read_azure_keys
from backend.src.ragas_eval import Evaluation as Ragas


def rejection_eval(
        question:str=None,
        answer:str=None,
        principle:str=None,
        sub_principle:str=None
    ):
    rejection_criteria = f"{CRITERIA_DICT.get(principle).get(sub_principle).get('prompt')}"
    if rejection_criteria:
        # Using the str.format method
        rejection_criteria = rejection_criteria.format(answer=answer,
                                                       question=question)
    try:
        eval_score = int(call_openai.ask(rejection_criteria))

    except Exception as e:
        eval_score = 0
        eval_reasoning = f"Something went wrong..{e}"
        rejection_criteria = f"Something went wrong..{e}"
    return answer, eval_score


def ensemble_eval(
        question_list:list=[],
        answer_list:list=[],
        principle:str=None,
        sub_principle:str=None
    ):
    original_question = question_list[0].split(" ", 1)[1]
    answers_str = [f"\n - {answer}" for i,answer in enumerate(answer_list, start=1)]
    answers_str = "".join(answers_str)

    same_answer_criteria = f"{CRITERIA_DICT.get(principle).get(sub_principle).get('prompt')}"
    if same_answer_criteria:
        same_answer_criteria = same_answer_criteria.format(
            original_question=original_question,
            answers_str=answers_str
        )
    eval_score = int(call_openai.ask(same_answer_criteria))

    return answer_list, eval_score

def langchain_criteria_eval(
        question:str=None,
        answer:str=None,
        principle:str=None,
        sub_principle:str=None,
    ):

    llm = llm_model.load_model()

    criteria = CRITERIA_DICT.get(principle).get(sub_principle).get('langchain_criteria')
    output_criteria = CRITERIA_DICT.get(principle).get(sub_principle).get('output_criteria')
    
    evaluator = load_evaluator("criteria", llm=llm, criteria=criteria)
    eval_result = evaluator.evaluate_strings(prediction=answer, input=question)
    #print(f"eval_result: \n {eval_result.get('reasoning')}")
    try:
        if eval_result.get('score')==None:
            eval_score = 1
        else:
            eval_score = int(eval_result.get('score'))
            evaluation_method = CRITERIA_DICT.get(principle).get(sub_principle).get('evaluation_method')
            if evaluation_method:
                # Using eval to compute the numerical operation
                eval_score = eval(evaluation_method.format(score=eval_score))
                reasoning = eval_result.get('reasoning')
        if reasoning:
            if sub_principle=="HARMFULNESS" and ("not harmful" or "there is no relevance" or "not contain harmful" in reasoning.lower()):
                eval_score=1
            if sub_principle=="SENSITIVE INFORMATION" and ("no sensitive" or "not contain any sensitive" or "no mention of any sensitive "in reasoning.lower()):
                eval_score=1
        
    except Exception as e:
        eval_score = 0
        #eval_reasoning = "Unable to process"
        eval_reasoning = ""

    return answer, eval_score


def ragas_metric_eval(question_list:str, answer_list:str, context_list:str):
    if (context_list == [] or context_list == [None]):
        eval_score = 0
    else:
        ragas_eval = Ragas()
        dataset_dict = {
            'question': question_list,
            'answer': answer_list,
            'contexts' : context_list,
        }
        eval_score = ragas_eval.evaluate(
            question=dataset_dict['question'][0],
            answer=dataset_dict['answer'][0],
            context=dataset_dict['contexts'][0]
        )

        # eval_score = 1 if eval_score>=0.5 else eval_score

    return answer_list[0], eval_score


def transparancy_eval(question_list, answer_list, sub_principle:str, context_list:str, title_list:list = [], section_list = []):
    criteria = CRITERIA_DICT.get("TRANSPARENCY").get(sub_principle).get('description')
    evaluation_method = CRITERIA_DICT.get("TRANSPARENCY").get(sub_principle).get('evaluation_method')
    if evaluation_method:
        # Using eval to evaluate the condition
        eval_score = eval(evaluation_method.format(context_list=context_list,
                                               title_list=title_list,
                                               section_list=section_list))
        eval_score=int(eval_score)

    if eval_score==1:
        reasoning = CRITERIA_DICT.get("TRANSPARENCY").get(sub_principle).get('evaluation_method_positive_reason')
    else:
        reasoning = CRITERIA_DICT.get("TRANSPARENCY").get(sub_principle).get('evaluation_method_negative_reason')
            
    return reasoning, eval_score