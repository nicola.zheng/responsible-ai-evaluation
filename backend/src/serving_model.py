from typing import Optional, List
from pydantic import BaseModel, Field


class AugmentationRequest(BaseModel):
    principle: List[str] 
    sub_principle: List[str]
    question: Optional[str] = None
    optional_field: Optional[str] = None


class ValidateListItem(BaseModel):
    principle: str = Field(..., example="FAIRNESS")
    sub_principle: str = Field(..., example="EQUITY")
    question_list: List[str] = Field(..., example=["chi deve pagare il canone rai?"])
    answer_list: List[str] = Field(..., example=["answer 1"])
    context_list: List[str] = Field([], example=["context 1"])
    title_list: List[str] = Field([], example=["title 1"])
    section_list: List[str] = Field([], example=["section 1"])

class ValidationRequest(BaseModel):
    list_to_validate: List[ValidateListItem] = Field(...)