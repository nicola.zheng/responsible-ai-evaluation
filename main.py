from fastapi import FastAPI, Request, Header, Query, Form, HTTPException
from fastapi.responses import HTMLResponse, PlainTextResponse, JSONResponse
from fastapi.templating import Jinja2Templates
from pydantic import BaseModel
from fastapi.openapi.utils import get_openapi
from fastapi.middleware.cors import CORSMiddleware
from starlette.middleware.base import BaseHTTPMiddleware
from fastapi.staticfiles import StaticFiles
from backend.config.principles_config import PRINCIPLES_FOR_AUGMENTATION, PRINCIPLES_STRAIT_TO_LLM
from backend.config.solution_config import solution_dict
from backend.config.principles_config import CRITERIA_DICT
from backend.src.principle_eval import PrincipleEval
from backend.src.serving_model import AugmentationRequest, ValidateListItem, ValidationRequest
from backend.utils.read_secret_manager import get_secret_value, read_login_credentials
from typing import List, Optional
import uuid
import json


app = FastAPI()
origins = ["*"]

# Enable CORS if required
app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],  # Adjust as needed
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)
 
# # Configure logging
# logging.basicConfig(level=logging.INFO)
# logger = logging.getLogger(__name__)
 
# class LogRequestMiddleware(BaseHTTPMiddleware):
#     async def dispatch(self, request: Request, call_next):
#         logger.info(f"Request URL: {request.url}")
#         logger.info(f"Request Headers: {request.headers}")
#         # Read the request body
#         body = await request.body()
#         logger.info(f"Request Body: {body.decode('utf-8')}")
#         response = await call_next(request)
#         return response
    
# app.add_middleware(LogRequestMiddleware)


app.mount("/frontend/static", StaticFiles(directory="frontend/static"), name="static")
app.mount("/frontend/pages", StaticFiles(directory="frontend/pages"), name="pages")

templates = Jinja2Templates(directory="frontend/pages")

@app.get('/', response_class=HTMLResponse)
async def index(request: Request):
    context = {"request": request, "solution_dict": solution_dict, "CRITERIA_DICT": CRITERIA_DICT}
    return templates.TemplateResponse("index.html", context)

@app.get('/update_domains/', response_class=HTMLResponse)
async def update_domains(request: Request, client:str=Query(...), hx_request: Optional[str] = Header(None)):
    if hx_request:
        domains = solution_dict.get(client, [])
        context = {"request": request, "domains": domains}
        return templates.TemplateResponse("domains.html", context)
    else:
        print("not htmx request")

@app.post('/evaluate', response_class=HTMLResponse)
async def evaluate(
    request: Request, 
    principles: List[str] = Form(...), 
    client: Optional[str] = Form(None), 
    domain: Optional[str] = Form(None), 
    input_question: str = Form(...),
    ext_endpoint: Optional[str] = Form(None), 
    ):
    new_uuid = uuid.uuid4()
    print(f'External endpoint: {ext_endpoint}')
    principle_evaluation = PrincipleEval(
                    uuid =new_uuid,
                    principles=principles,
                    question=input_question,
                    client_qa=client,
                    domain_qa=domain,
                    get_answer_endpoint=ext_endpoint
                )
    evaluation_results = principle_evaluation.run()
    print(f'Dict for HTMX:\n{evaluation_results}')
    context = {"request": request, "evaluation_results": evaluation_results}
    #return templates.TemplateResponse("eval_results_table_format.html", context)
    return templates.TemplateResponse("evaluation_results.html", context)


@app.post("/login", response_class=PlainTextResponse)
async def login(username: str = Form(...), password: str = Form(...)):
    print(f"l'utente {username} ha provato ad accedere")
    secret_dict = get_secret_value(secret_name="login-respAI-tool")
    username_list, secret_password = read_login_credentials(secret_dict, htmx_username=username, htmx_password=password)

    # Strip the domain part if there is an '@' in the username
    if '@' in username:
        username = username.split('@')[0]

    username = username.lower()
    
    if username in username_list:
        if password==secret_password:
            print(f"l'utente {username} ha fatto il login con successo")
            return "OK"
        else:
            return "Invalid password"
    else:
        return "Invalid username"


### SERVING: CALL EXTERNAL MODEL ###
def custom_openapi():
    with open("./backend/utils/serving_openapi.json", "r") as openapi:
        return json.load(openapi)
app.openapi = custom_openapi

### SERVING: CALLED BY EXTERNAL MODEL ###
## TODO: insert auth before serving

@app.post("/augmentation")
async def augment(request: AugmentationRequest):
    try:
        if len(request.principle) != len(request.sub_principle):
            raise HTTPException(status_code=400, detail={"error":"The number of principles and sub_principles must be the same."})
    
        if len(request.principle)>=1 and len(request.sub_principle)>=1:
            principle = [item.upper() for item in request.principle]
            sub_principle = [item.upper() for item in request.sub_principle]

            print('-'*15 + ' '*5 +f'Request to /augmentation for {principle} - {sub_principle}' + ' '*5 + '-'*15)
            response_list = []
            for principle_str, sub_principle_str in zip(principle, sub_principle):
                if (principle_str, sub_principle_str) in PRINCIPLES_FOR_AUGMENTATION:
                    flag_augmentation = True

            if flag_augmentation:
                print("At least one combination requires augmentation")
                if request.question:
                    question = request.question
                    for principle_str, sub_principle_str in zip(principle, sub_principle):
                        if (principle_str, sub_principle_str) in PRINCIPLES_FOR_AUGMENTATION:
                            
                                principle_evaluation = PrincipleEval(principle, sub_principle, question)
                                print(f'question_generation for {principle_str} - {sub_principle_str}')
                                question_list = principle_evaluation.question_generation(principle_str, sub_principle_str, question_to_test=question)
                                response_list.append({
                                    "principle": principle_str,
                                    "sub_principle": sub_principle_str,
                                    "list": question_list
                                })
                        elif (principle_str, sub_principle_str) in PRINCIPLES_STRAIT_TO_LLM:
                            response_list.append({
                                    "principle": principle_str,
                                    "sub_principle": sub_principle_str,
                                    "list": [question]
                            })
                        else:
                            raise HTTPException(status_code=400,
                                                detail={"error": f"The {principle_str}/{sub_principle_str} combination does not exist"})
                    response = JSONResponse(content={
                        "status": "200",
                        "esito": "OK",
                        "question_list": response_list
                        }
                    )
                    print(f'Response:\n {response.body}')
                    return response
                else:
                    raise HTTPException(status_code=400, 
                                        detail={"error": f"For these principles combination, the parameter question is mandatory"})
            else:
                response = JSONResponse(content={"status": "200", "esito": "KO", "detail": f"These principles do not need augmented questions."})
                print(f'Response:\n {response.body}')
                return response
        else:
            raise HTTPException(status_code=400,
                                detail={"error": "principle and sub_principle must have at least one element each"})

    except ValueError as e:
        # Handle potential errors during JSON parsing
        raise HTTPException(status_code=400, detail=str(e))




@app.post("/validation")
async def validation(request: ValidationRequest):
    try:
        response_list = []
        for item in request.list_to_validate:
            if not item.principle or not item.sub_principle:
                raise HTTPException(status_code=400, detail={"error":"The number of principles and sub_principles must be the same."})
            else:
                principle = item.principle.upper()
                sub_principle = item.sub_principle.upper()
            
            print('-'*15 + ' '*5 +f'Request to /augmentation for {principle} - {sub_principle}' + ' '*5 + '-'*15)
            if (principle, sub_principle) in PRINCIPLES_FOR_AUGMENTATION or (principle, sub_principle) in PRINCIPLES_STRAIT_TO_LLM:
                question_list = item.question_list
                answer_list = item.answer_list
                context_list = context_list if item.context_list else [None]
                title_list = title_list if item.title_list else [None]
                section_list = section_list if item.section_list else [None]
                principle_evaluation = PrincipleEval(principle, sub_principle)
                score, reasoning, criteria = principle_evaluation.eval_principle(principle=principle, sub_principle=sub_principle,
                                                                        question_list=question_list,
                                                                        answer_list=answer_list,
                                                                        context_list=context_list,
                                                                        title_list=title_list,
                                                                        section_list=section_list)
                reasoning = reasoning.strip()
                reasoning = reasoning.replace("**", "")
                reasoning = reasoning.replace("\n", " ")
                response_list.append({
                    "principle": principle,
                    "sub_principle": sub_principle,
                    #"criteria": criteria,
                    "score": score,
                    "reasoning": reasoning
                })
            
            else:
                raise HTTPException(status_code=400,
                                    detail={"error": f"The {principle}/{sub_principle} combination does not exist"})

        return response_list
            
    except ValueError as e:
        # Handle potential errors during JSON parsing
        raise HTTPException(status_code=400, detail=str(e))