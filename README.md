# Directory Structure

```
backend
└───src
    └───config
    └───utils

frontend
└───pages
└───static
    └───images
    └───script
    └───style

```
### FE ENTRY POIN
```sh
uvicorn main:app --host=0.0.0.0 --port=8080
```

### BE ENTRY POINT

```sh
uvicorn main:app --host=0.0.0.0 --port=8080
```

# Evaluation Process
Starting from `principle_eval.py` for each principle let's run `.eval_principle()` method.
