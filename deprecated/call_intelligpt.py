import requests
import time
import os
from dotenv import load_dotenv

CONTEXT_SEP = '\n'
load_dotenv()
intelligpt_token = os.getenv("INTELLIGPT_TOKEN")



def ask(
        question: str,
        context:str = 'afast',
        domain:str = 'general'
    ):
    url = 'https://dynamic-webapp-be.azurewebsites.net/chatbot/'
    
    header_request = {
      "Content-Type": "application/json",
      "Accept": "application/json",
      "Authorization": "Bearer {token}".format(token = intelligpt_token)
    }

    body_request = {
      "client": context,
      "domain": domain,
      "question": question,
      "session_id": ""
    }

    if context == 'test' and domain == 'test':
        time.sleep(5)
        intelli_gpt_response = 'nessuna risposta'
        intelli_gpt_context = ['contesto1', 'contesto2']
        intelli_gpt_title = None
        intelli_gpt_section = None
    else:
      response = requests.post(url, headers=header_request, json = body_request)
      intelli_gpt_response = response.json().get("answer")
      intelli_gpt_context = response.json().get("body")
      intelli_gpt_title = response.json().get("titles")
      intelli_gpt_section = response.json().get("sections")
    return intelli_gpt_response, intelli_gpt_context, intelli_gpt_title, intelli_gpt_section

if __name__== "__main__":
  print(
    ask(context="afast", domain="general", question="chi la vigila la previdenza complementare?")
  )
