from fastapi import FastAPI
from pydantic import BaseModel, Field
from backend.utils.call_endpoint import EndpointGetAnswer
import requests
import json

from backend.utils.read_secret_manager import get_secret_value, read_intelligpt_login

class Question(BaseModel):
    question: str

app = FastAPI()

@app.get("/")
async def read_root():
    return {"Endpoint:" "http://0.0.0.0:3000/get_answer"}

### DEPRECATED
# @app.post("/get_answer")
# async def validation(request: Question):
#     #print('#'*8, ' '*3, 'In /get_answer', ' '*3, '#'*8)
#     secret_dict = get_secret_value(secret_name="intelligpt_login")
#     intelligpt_token= read_intelligpt_login(secret_dict=secret_dict, key="INTELLIGPT_TOKEN")
#     intelligpt_url= read_intelligpt_login(secret_dict=secret_dict, key="INTELLIGPT_QA_ENDPOINT")
                                           
#     endpoint = EndpointGetAnswer(
#         endpoint_url=intelligpt_url, 
#         token=intelligpt_token
#     )
#     request_body = {}
#     request_body["question"] = request.question
#     request_body["client"] = endpoint.context
#     request_body["domain"] = endpoint.domain
#     request_body["session_id"] = ""

#     #print(f'Request Body:\n{request_body}')

#     request_response = requests.post(endpoint.endpoint_url, json = request_body)
#     response = {
#         "success": True,
#         "response": request_response.json().get("response")
#     }
#     #print(f'IntelliGPT Response:\n{response}')
#     return response